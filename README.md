# MOPCON API

## 安裝
1. clone 專案
```
git clone git@gitlab.com:mopcon/mopcon-api.git
```
2. 複製 .env.example 至 .env
```
cp .env.example .env
```
3. 至 .env 設定資料庫登入資訊，請將以下設定依照開發環境修改。
```
APP_ENV=develop
RESOURCE_PATH
FACEBOOK_TOKEN
SHEET_2019_KEY
NEWS_2019_ID
```
4. 安裝 PHP 相依套件。
```
composer install
```

5. 啟動 php 內建 server
```
$ php -S localhost:8000 -t public
```


## 設定時區

於 .env 中設置時區

```
APP_TIMEZONE=Asia/Taipei
```