<?php

namespace App\Http\Controllers;

class HealthController
{
    use ApiTrait;

    /**
     * 確認 server 連線狀態
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return $this->returnSuccess('Success.');
    }
}
