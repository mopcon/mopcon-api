# health API

確認 server 狀態

- URL

  /health

- Method

  `GET`

- URL Params

  null

- Data Params

  null

- Success Response

  - Code: 200
  - Content:

  ```json
  {
    "success": true,
    "message": "Success.",
    "data": []
  }
  ```
